class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name, @last_name = first_name.capitalize, last_name.capitalize

    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    raise "there is a conflict with an existing course" if course_conflict?(course)

    self.courses << course
    course.students << self
  end

  def course_load
    load = Hash.new(0)
    self.courses.each do |course|
      load[course.department] += course.credits
    end
    load
  end

  def course_conflict?(check_course)
    self.courses.any? {|course| check_course.conflicts_with?(course)}
  end
end
